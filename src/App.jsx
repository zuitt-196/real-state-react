// IMPORTH THE COMPOENT
import Header from "./components/header/Header";
import Hero from "./components/hero/Hero";

import './App.css'
import Compnanies from "./components/Companies/Compnanies";
import Residence from "./components/Residences/Residence";
import Value from "./components/Value/Value";
import Contact from "./components/Contact/Contact";
import GetStarted from "./components/GetStarted/GetStarted";
import Footer from "./components/Footer/Footer";


function App() {
  return (
    <div className="App">
      <div>
          <div className="white-gradient"/>
          <Header />
          <Hero />
      </div>
        <Compnanies/>
        <Residence/>
        <Value/>
        <Contact/>
        <GetStarted/>
        <Footer/>
  
    </div>
  );
}

export default App;
