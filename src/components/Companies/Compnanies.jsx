import React from 'react'
import './companies.css'
import companies1 from '../../image/companies-1.png';
import companies2 from '../../image/companies-2.jpg';
import companies3 from '../../image/companies-3.png';
import companies4 from '../../image/companies-4.jpg';

const Compnanies = () => {
  return (
        <section className="c-wrapper">
                <div className="paddings innerWidth flexCenter c-container">
                        <img src={companies1} alt="c-1" />
                        <img src={companies2} alt="c-2" />    
                        <img src={companies3} alt="c-3" />    
                        <img src={companies4} alt="c-4" />    
                </div>          
        </section>
  )
}

export default Compnanies