import React from 'react'
import './Footer.css'
import {BsFillHouseAddFill} from 'react-icons/bs'
const Footer = () => {
  return (
<section className="f-wrpper">
        <div className="paddings innerWidth flexCenter f-container">
                {/* LEFT SIDE */}
                        <div className="flexColStart f-left">
                                <span>Real state <BsFillHouseAddFill size={20} style={{ color: "green" }} /> </span>
                                <span className="secondaryText">
                                        Our vision is to make all people <br />
                                        the best place to live for them
                                </span>
                        </div>

                {/* RIGHT SIDE  */}
                <div className="flexColStart f-right">
                                <span className="primaryText">Information</span>
                                <span className="secondaryText">Davao city, philippines</span>

                                <div className="flexCenter f-menu">
                                        <span>Property</span>
                                        <span>Services</span>
                                        <span>Product</span>
                                        <span>About Us</span>
                                </div>
                </div>
                
                 
        </div>
</section>
  )
}

export default Footer