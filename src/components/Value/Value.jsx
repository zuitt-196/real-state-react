import React from 'react'
import './Value.css';
import value from '../../image/r2.jpg'
import {useState} from 'react'
import { Accordion ,  
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
    AccordionItemState
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import data from  '../../utils/accordion'
import { MdOutlineArrowDropDown } from 'react-icons/md';

const Value = () => {
        // DEFINE STATE
        const [ClassName, setClassName] = useState(null)
  return (
        <section className="v-wrapper">
                <div className="paddings innerWidth flexCenter v-container">
                    {/* LEFT SIDE} */}
                    <div className="v-left">
                            <div className="image-container">   
                                    <img src={value} alt="value" />
                            </div>
                    </div>

                    {/* RIGHT SIDE */}
                    <div className="flexColStart v-right ">
                            <span className="orangeText">Our value</span>
                            <span className="primaryText">Value we give  to you</span>
                            <span className="secondaryText">
                                We always ready to help by providing the best services for you. <br />
                                we beleive a good blace to live can make your life better
                            </span>    

                            {/* DEFIBE THE COMMENT USED OF LIBRARY Accordion */}
                            <Accordion 
                                className='accordion'
                                allowMultipleExpanded={false}
                                preExpanded={[0]}
                             >
                                {
                                data.map((item , i) => {
                                        return (

                                <AccordionItem className={`accordionItem ${ClassName}`} key={i} uuid={i}>
                                                <AccordionItemHeading>
                                                <AccordionItemButton>

                                                                <AccordionItemState>
                                                                                {({expanded}) => expanded ? setClassName('expanded'): setClassName("collapsed")}
                                                                </AccordionItemState>

                                                        <div className='flexCenter icon'>
                                                                        {item.icon}
                                                        </div>

                                                        <span className='primaryText'>{item.heading}</span>
                                                        <div className='flexCenter icon'>
                                                                <MdOutlineArrowDropDown size={20}/>
                                                        </div>
                                                </AccordionItemButton>
                                                </AccordionItemHeading>
                                                <AccordionItemPanel>
                                                          <p className="secondaryText">{item.details}</p>                                                   
                                                </AccordionItemPanel>
                                        </AccordionItem>                
                                        )
                                })
                                }
                            </Accordion>
                    </div>
                </div>
        </section>
  )
}

export default Value