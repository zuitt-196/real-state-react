import React from 'react'
import './hero.css';
import {GiThumbUp}  from 'react-icons/gi'
import {HiLocationMarker} from 'react-icons/hi'
import CountUp from 'react-countup';
import Herobanner from '../../image/hero-image.jpg';
import {motion} from 'framer-motion'

const Hero = () => {
    return (
        <section className="hero-wrapper">
            <div className="paddings innerWidth flexCenter hero-container">
                {/* LEFT SIDE */}
                <div className="flexColStart hero-left">
                    <div className="hero-title">
                            <div className="orange-circle"/>    
                            <motion.h1 
                                initial={{y: "2rem", opacity:0}}
                                animate={{y:0, opacity:1}}
                                transition={{
                                        duration:2,
                                        type:'spring'
                                }}
                            >Discover <br />most Suitable <br /> property in  
                            <br /> <i style={{color: 'blue', fontWeight: 'bold', borderBottom: '1px solid'}}>
                            Davao city</i><GiThumbUp style={{color: 'gold'}} size={25}/></motion.h1>
                    </div>

                    <div className="flexColStart hero-description">
                            <span className='secondaryText'>
                                 Find a variety of properties that suit your very easilty
                            </span>
                            <span className='secondaryText'>Forgety all difficulties in finding a residence for you</span>
                    </div>

                    <div className="flexCenter search-bar">
                                <HiLocationMarker color= "var(--blue)" size={25}/>
                                <input type="text" />
                                <button className='button'>Search</button>
                    </div>

                    <div className='flexCenter stats'>
                            <div className='flexColCenter stat'>
                                <span>
                                    <CountUp start={8800} end={9000} duration={4}/>
                                    <span>+</span>
                                </span>
                                <span className='secondaryText'>premiun Products</span>
                             </div>


                             <div className='flexColCenter stat'>
                                <span>
                                    <CountUp start={1950} end={2000} duration={4}/>
                                    <span>+</span>
                                </span>
                                <span className='secondaryText'>Happy Customers</span>
                             </div>
                             
                             <div className='flexColCenter stat'>
                                <span>
                                    <CountUp end={28}/>
                                    <span>+</span>
                                </span>
                                <span className='secondaryText'>Award Winning</span>
                             </div>

                    </div>

                </div>
                {/* RIGHT SIDE */}
                <div className='flexCenter hero-right'>
                    <motion.div 
                        initial={{x: "7rem", opacity:0}}
                        animate={{x: 0, opacity:1}}
                        transition={{
                            duration:2
                        }}
                    className="image-container">
                        <img src={Herobanner} alt="hero" />
                    </motion.div>
                </div>

            </div>
        </section>
    )
}

export default Hero