import React,{useState} from 'react'
import { BsFillHouseAddFill } from "react-icons/bs"
import OutsideClickHandler from 'react-outside-click-handler';
import './header.css'
import {HiMenuAlt3} from 'react-icons/hi';

const Header = () => {
    const [menuOpened, setMunuOpened ] = useState(false);
  
    const getMenuStyle = (menuOpened) =>{
            if (document.documentElement.clientWidth <= 800) {
                return {
                        right: !menuOpened && '-100%'
                }
            }
    }
    return (
        <section className="h-wrapper">
            <div className="flexCenter paddings innerWidth h-container">
                <span>Real state <BsFillHouseAddFill size={20} style={{ color: "green" }} /> </span>
                <OutsideClickHandler onOutsideClick={ () =>{
                        setMunuOpened(false)
                }} >
                <div className='flexCenter h-menu' style={getMenuStyle(menuOpened)}>
                    <a href="#">Residences</a>
                    <a href="#">Our values</a>
                    <a href="#">Contact us</a>
                    <a href="#">Get started</a>
                    <button className='button'>
                        <a href="#">Contact</a>
                    </button>
                </div>    
           
                </OutsideClickHandler>
                  {/* ICON MENU */}
                    <div className='menu-icon' onClick={() =>setMunuOpened((prevState) => !prevState)}>
                            <HiMenuAlt3 size={30}/>
                    </div>
            </div>
          
        </section>
    )
}

export default Header