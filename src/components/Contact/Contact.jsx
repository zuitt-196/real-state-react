import React from 'react'
import './Contact.css'
import {MdCall} from 'react-icons/md';
import {BsFillChatDotsFill} from 'react-icons/bs'
import {BiMessage} from 'react-icons/bi'
const Contact = () => {
  return (
        <section className='c-wrapper'>
                <div className='paddings innerWidth flexCenter c-container'>
                        {/* LEFT SIDE */}
                    <div className='flexColStart c-left'>
                        <span className='orangeText'>Our contacts</span>
                        <span className='primaryText'>Easy to Contact</span>
                        <span className="secondaryText"> always ready to help by providing the best service believe a good 
                            blace to live can make your lif e better
                        </span>

                            <div className='flexColStart contactModes'>
                                    {/* first row */}
                                    <div className='flexStart row'>
                                            <div className='flexColCenter mode'>
                                                    <div className='flexStart'>
                                                            <div className="flexCenter icon">
                                                                <MdCall size ={25}/>
                                                            </div>
                                                            <div className='flexColStart detail'>
                                                                    <span className="primaryText">Call</span>
                                                                    <span>021 123 145 14</span>
                                                            </div>
                                                    </div>
                                                    <div className="flexCenter button">
                                                            Call now
                                                    </div>
                                            </div>

                                        {/* second mode  */}
                                        <div className='flexColCenter mode'>
                                                    <div className='flexStart'>
                                                            <div className="flexCenter icon">
                                                                <BsFillChatDotsFill size ={25}/>
                                                            </div>
                                                            <div className='flexColStart detail'>
                                                                    <span className="primaryText">Chat</span>
                                                                    <span>021 123 145 14</span>
                                                            </div>
                                                    </div>
                                                    <div className="flexCenter button">
                                                            Chat now
                                                    </div>
                                            </div>

                                            
                                    </div>


                                    {/* Second row */}
                                    <div className='flexStart row'>
                                            <div className='flexColCenter mode'>
                                                    <div className='flexStart'>
                                                            <div className="flexCenter icon">
                                                                <MdCall size ={25}/>
                                                            </div>
                                                            <div className='flexColStart detail'>
                                                                    <span className="primaryText">Video Call</span>
                                                                    <span>021 123 145 14</span>
                                                            </div>
                                                    </div>
                                                    <div className="flexCenter button">
                                                            Call now
                                                    </div>
                                            </div>

                                        {/* second mode  */}
                                        <div className='flexColCenter mode'>
                                                    <div className='flexStart'>
                                                            <div className="flexCenter icon">
                                                                <BiMessage size ={25}/>
                                                            </div>
                                                            <div className='flexColStart detail'>
                                                                    <span className="primaryText">Message</span>
                                                                    <span>021 123 145 14</span>
                                                            </div>
                                                    </div>
                                                    <div className="flexCenter button">
                                                            Chat now
                                                    </div>
                                            </div>

                                    </div>
                            </div>

                    </div>

                    {/* RIGHT SIDE */}
                    <div className='c-right'>
                        <div className='image-container'>
                                <img src="https://images.unsplash.com/photo-1580587771525-78b9dba3b914?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTB8fHJlYWwlMjBlc3RhdGV8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60" alt="contact" />
                        </div>
                    </div>
                </div>
        </section>
  )
}

export default Contact
