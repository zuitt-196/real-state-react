import React from 'react'
import { Swiper, SwiperSlide, useSwiper} from 'swiper/react';
import 'swiper/css';
import './Residence.css'
import data from '../../utils/slide.json'
import {sliderSettings} from '../../utils/common'
 
const Residence = () => {
  return (
        <section className='r-wrapper'>
            <div className= "paddings innerWidth r-container">
                  <div className="flexColStart r-head ">
                        <span className="orangeText">Best choices</span>
                        <span className="primaryText">Popular Residence</span>
                  </div>
                  <Swiper {...sliderSettings}>
                        <SliderButtons/>
                        {
                              data.map((card, i) =>(
                                    <SwiperSlide key={i}>
                                                <div className="flexColStart r-card">
                                                      <img src={card.image} alt="home" />
                                                      <span className="secondaryText r-price">
                                                                  <span style={{color: "orange"}}>$</span>
                                                                  <span>{card.price}</span>
                                                      </span>
                                                      <span className='primaryText'>{card.name}</span>
                                                      <span className='secondaryText      '>{card.details}</span>
                                                </div>
                                    </SwiperSlide>
                              ))
                        }
                  </Swiper>
            </div>
        </section>
  )
}

export default Residence

// DEFINE THE COMPNONENT OF BUTTONS FOR SLIDER
const  SliderButtons=() =>{
      const swiper = useSwiper();
return(
      <div className="flexCenter r-buttons">
            <button onClick={(e) => swiper.slidePrev()}>&lt;</button>
            <button  onClick={(e) => swiper.slideNext()}>&gt;</button>
      </div>
)
}
